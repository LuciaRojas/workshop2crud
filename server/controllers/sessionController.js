const Session = require("../models/sessionModel");

const User = require("../models/userModel");

//sessionStorage.setItem("lastname", "Smith");

const sessionPost = (req, res) => {
    var session = new Session();
    var user = new User();
  
    session.usuario = req.body.usuario;
    session.sessioID = req.body.sessioID;
  
    if (session.usuario && session.sessioID) {

      session.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the session', err)
          res.json({
            error: 'There was an error saving the session'
          });
        }
        res.status(201);//CREATED
        res.header({
          'location': `http://localhost:3000/api/session/?id=${session.id}`
        });
        res.json(session);
      });
    } else {
      res.status(422);
      console.log('error while saving the session')
      res.json({
        error: 'No valid data provided for session'
      });
    }
  };
  
  
/**
 * Get all users
 *
 * @param {*} req
 * @param {*} res
 */
const sessionGet = (req, res) => {
  // if an specific user is required

  let sessioID= req.query.sessioID;
console.log(sessioID);
  if (req.query && sessioID) {
    Session.findOne({sessioID:sessioID}, function (err, session) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "session doesnt exist" })
      }

      User.findById(sessioID,function(err,user){
        if (err) {
          res.status(404);
          console.log('error while queryting the task', err)
          res.json({ error: "session doesnt exist" })
        }
        res.json(user);
      });
    });
  }
};

  module.exports = {
    sessionPost,
    sessionGet
   
}