const Task = require("../models/taskModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const taskPost = (req, res) => {
  var task = new Task();

  task.nombre = req.body.nombre;
  task.apellido = req.body.apellido;
  task.correo = req.body.correo;
  task.direccion = req.body.direccion;

 // nombre: { type: String },
  //apellido: { type: String },
 // correo: { type: String },
  //direccion: { type: String }
  //console.log(task.nombre);
  //console.log(task.apellido);
  //console.log(task.correo);
  //console.log(task.direccion);

  if (task.nombre && task.apellido && task.correo && task.direccion) {
    task.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the task', err)
        res.json({
          error: 'There was an error saving the task'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/tasks/?id=${task.id}`
      });
      res.json(task);
    });
  } else {
    res.status(422);
    console.log('error while saving the task')
    res.json({
      error: 'No valid data provided for task'
    });
  }
};

/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const taskGet = (req, res) => {
  // if an specific task is required
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }
      res.json(task);
    });
  } else {
    // get all tasks
    Task.find(function (err, tasks) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(tasks);
    });

  }
};

/**
 * Updates a task
 *
 * @param {*} req
 * @param {*} res
 */
const taskPatch = (req, res) => {
  // get task by id
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }

      // update the task object (patch)
      task.nombre = req.body.nombre ? req.body.nombre : task.nombre;
      task.apellido = req.body.apellido ? req.body.apellido : task.apellido;
      task.correo = req.body.correo ? req.body.correo : task.correo;
      task.direccion = req.body.direccion ? req.body.direccion : task.direccion;

      //task.nombre = req.body.nombre;
      //task.apellido = req.body.apellido;
      //task.correo = req.body.correo;
      //task.direccion = req.body.direccion;

      
      // update the task object (put)
      // task.title = req.body.title
      // task.detail = req.body.detail

      task.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the task', err)
          res.json({
            error: 'There was an error saving the task'
          });
        }
        res.status(200); // OK
        res.json(task);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" })
  }
};




const taskDelete = (req, res) => {
  if (req.query && req.query.id) {
      Task.findByIdAndRemove(req.query.id, function(err, task) {
          if (err) {
              res.status(404);
              console.log('Error while queryting the Student', err)
              res.json({ error: "Student doesnt exist" })
          }
          res.json(task);
      });
  } else {
      // get all student
      Task.findByIdAndRemove(function(err, task) {
          if (err) {
              res.status(500);
              res.json({ "Error": err });
          }
          res.json(task);
      });

  }
};


module.exports = {
  taskGet,
  taskPost,
  taskPatch,
  taskDelete
}