const User = require("../models/userModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req, res) => {
  var user = new User();

  user.usuario = req.body.usuario;
  user.contrasenna = req.body.contrasenna;
  user.nombre = req.body.nombre;
  user.apellido = req.body.apellido;

 // nombre: { type: String },
  //apellido: { type: String },
 // correo: { type: String },
  //direccion: { type: String }
  //console.log(task.nombre);
  //console.log(task.apellido);
  //console.log(task.correo);
  //console.log(task.direccion);

  if (user.usuario && user.contrasenna && user.nombre && user.apellido) {
    user.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/users/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};

/**
 * Get all users
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req, res) => {
  // if an specific user is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "User doesnt exist" })
      }
      res.json(user);
    });
  } else {
    // get all tasks
    User.find(function (err, users) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      console.log(users);
      res.json(users);
    });

  }
};

/**
 * Updates a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPatch = (req, res) => {
  // get user by id
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }

      // update the student object (patch)
      user.usuario = req.body.usuario ? req.body.usuario : user.usuario;
      user.contrasenna = req.body.contrasenna ? req.body.contrasenna : user.contrasenna;
      user.nombre = req.body.nombre ? req.body.nombre : user.nombre;
      user.apellido = req.body.apellido ? req.body.apellido : user.apellido;

      //user.nombre = req.body.nombre;
      //user.apellido = req.body.apellido;
      //user.correo = req.body.correo;
      //user.direccion = req.body.direccion;

      
      // update the user object (put)
      // user.title = req.body.title
      // user.detail = req.body.detail

      user.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the user', err)
          res.json({
            error: 'There was an error saving the user'
          });
        }
        res.status(200); // OK
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist" })
  }
};




const userDelete = (req, res) => {
  if (req.query && req.query.id) {
    User.findByIdAndRemove(req.query.id, function(err, user) {
          if (err) {
              res.status(404);
              console.log('Error while queryting the User', err)
              res.json({ error: "User doesnt exist" })
          }
          res.json(user);
          res.status(204).send();
      });
  } 

};
/*

const authenti = (req, res) => {
   var user = new User();

  user.usuario = req.body.usuario;
  user.contrasenna = req.body.contrasenna;


  if (user.usuario && user.contrasenna ) {
    console.log('entro');
    res.json('entro');
      // get all tasks
      User.find(function (err, users) {
        console.log( users.contrasenna);
        console.log(users);
        res.json(users);


      });

  } else {
    res.status(422);
    console.log('error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};



const authenti = (req, res) => {
  var user = new User();
 //  Now find the user by their email address
 let user = await User.findOne({ usuario: req.body.usuario });
 if (!user) {
     return res.status(400).send('Incorrect usuario or password.');
 }

 // Then validate the Credentials in MongoDB match
 // those provided in the request
 const validPassword = await bcrypt.compare(req.body.contrasenna, user.contrasenna);
 if (!validcontrasenna) {
     return res.status(400).send('Incorrect usuario or contrasenna.');
 }

 res.send(true);
};

*/

module.exports = {
    userGet,
    userPost,
    userPatch,
 
    userDelete
}