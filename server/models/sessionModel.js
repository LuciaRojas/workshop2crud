const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const session = new Schema({
  usuario: { type: String },
  sessioID: { type: String },
  started:{type:Date, default:Date.now()}
  //nombre, apellido, correo electrónico, dirección
});

module.exports = mongoose.model('sessions', session);